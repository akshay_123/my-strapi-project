'use strict';

/**
 * doc-component service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::doc-component.doc-component');
