'use strict';

/**
 *  doc-component controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::doc-component.doc-component');
