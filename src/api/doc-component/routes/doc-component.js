'use strict';

/**
 * doc-component router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::doc-component.doc-component');
